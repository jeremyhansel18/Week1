import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SecondPage } from '../second/second';

/**
 * Generated class for the SignIn2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in2',
  templateUrl: 'sign-in2.html',
})
export class SignIn2Page implements OnInit{

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(){
    console.log(this.navParams.get('nama'));
  }

  goToSecondPages(){
    this.navCtrl.push(SecondPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignIn2Page');
  }

}
