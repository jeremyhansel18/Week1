import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignIn2Page } from './sign-in2';

@NgModule({
  declarations: [
    SignIn2Page,
  ],
  imports: [
    IonicPageModule.forChild(SignIn2Page),
  ],
})
export class SignIn2PageModule {}
