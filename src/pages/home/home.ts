import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SignInPage } from '../sign-in/sign-in';
import { SignIn2Page } from '../sign-in2/sign-in2';




@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  goToSecondPage(){
    this.navCtrl.push(SignInPage);
  }

  goToAnotherPage(){
    this.navCtrl.push(SignIn2Page, {nama: "hansel"});
  }
}
